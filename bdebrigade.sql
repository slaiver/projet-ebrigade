-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 10 mars 2020 à 20:14
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bdebrigade`
--

-- --------------------------------------------------------

--
-- Structure de la table `engins`
--

DROP TABLE IF EXISTS `engins`;
CREATE TABLE IF NOT EXISTS `engins` (
  `idEngins` int(11) NOT NULL AUTO_INCREMENT,
  `Nom_Engin` varchar(45) DEFAULT NULL,
  `Date_Heur_Depart` date DEFAULT NULL,
  `Date_Heure_Arriver` date DEFAULT NULL,
  `Date_Heure_Retour` date DEFAULT NULL,
  `kilométrage` int(50) NOT NULL,
  `affecte_a` varchar(50) NOT NULL,
  `date_fin_assurance` date NOT NULL,
  PRIMARY KEY (`idEngins`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `engins_personnel`
--

DROP TABLE IF EXISTS `engins_personnel`;
CREATE TABLE IF NOT EXISTS `engins_personnel` (
  `Engins_idEngins` int(11) NOT NULL,
  `Personnel_idPersonnel` int(11) NOT NULL,
  PRIMARY KEY (`Engins_idEngins`,`Personnel_idPersonnel`),
  KEY `fk_Engins_has_Personnel_Personnel1_idx` (`Personnel_idPersonnel`),
  KEY `fk_Engins_has_Personnel_Engins1_idx` (`Engins_idEngins`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `geographique`
--

DROP TABLE IF EXISTS `geographique`;
CREATE TABLE IF NOT EXISTS `geographique` (
  `idGeographique` int(11) NOT NULL AUTO_INCREMENT,
  `Position_X` int(11) DEFAULT NULL,
  `Position_Y` int(11) DEFAULT NULL,
  PRIMARY KEY (`idGeographique`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `geographique`
--

INSERT INTO `geographique` (`idGeographique`, `Position_X`, `Position_Y`) VALUES
(1, 5000, 200),
(2, 300, 500);

-- --------------------------------------------------------

--
-- Structure de la table `intervention`
--

DROP TABLE IF EXISTS `intervention`;
CREATE TABLE IF NOT EXISTS `intervention` (
  `idIntervention` int(11) NOT NULL AUTO_INCREMENT,
  `Numero_Intervention` int(11) NOT NULL,
  `Commune` varchar(45) DEFAULT NULL,
  `Adresse` varchar(45) DEFAULT NULL,
  `Type_interv` varchar(45) DEFAULT NULL,
  `Opm` tinyint(4) DEFAULT NULL,
  `Important` tinyint(4) DEFAULT NULL,
  `Date_Heure_Debut` datetime DEFAULT NULL,
  `Date_Heure_Fin` datetime DEFAULT NULL,
  `Geographique_idGeographique` int(11) NOT NULL,
  `Statut` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idIntervention`,`Geographique_idGeographique`),
  KEY `fk_Intervention_Geographique1_idx` (`Geographique_idGeographique`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `intervention`
--

INSERT INTO `intervention` (`idIntervention`, `Numero_Intervention`, `Commune`, `Adresse`, `Type_interv`, `Opm`, `Important`, `Date_Heure_Debut`, `Date_Heure_Fin`, `Geographique_idGeographique`, `Statut`) VALUES
(1, 100, 'lolol', NULL, NULL, NULL, NULL, '2020-03-04 00:00:00', '2020-03-06 00:00:00', 2, 'lol'),
(2, 200, 'mulhouse', NULL, NULL, 2, NULL, '2020-03-27 04:07:11', '2020-03-24 04:28:23', 1, 'ok');

-- --------------------------------------------------------

--
-- Structure de la table `intervention_engins`
--

DROP TABLE IF EXISTS `intervention_engins`;
CREATE TABLE IF NOT EXISTS `intervention_engins` (
  `Intervention_idIntervention` int(11) NOT NULL,
  `Engins_idEngins` int(11) NOT NULL,
  PRIMARY KEY (`Intervention_idIntervention`,`Engins_idEngins`),
  KEY `fk_Intervention_has_Engins_Engins1_idx` (`Engins_idEngins`),
  KEY `fk_Intervention_has_Engins_Intervention_idx` (`Intervention_idIntervention`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `parametre`
--

DROP TABLE IF EXISTS `parametre`;
CREATE TABLE IF NOT EXISTS `parametre` (
  `idParametre` int(11) NOT NULL AUTO_INCREMENT,
  `Jours_Feries` varchar(45) DEFAULT NULL,
  `Heure_Debut` datetime DEFAULT NULL,
  `Heure_Fin` datetime DEFAULT NULL,
  PRIMARY KEY (`idParametre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `personnel`
--

DROP TABLE IF EXISTS `personnel`;
CREATE TABLE IF NOT EXISTS `personnel` (
  `idPersonnel` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(45) DEFAULT NULL,
  `Role` varchar(45) DEFAULT NULL,
  `Responsable_idResponsable` int(11) NOT NULL,
  `Parametre_idParametre` int(11) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `sexe` varchar(2) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `civilite` varchar(10) NOT NULL,
  `date_engagement` date NOT NULL,
  PRIMARY KEY (`idPersonnel`,`Responsable_idResponsable`,`Parametre_idParametre`),
  KEY `fk_Personnel_Responsable1_idx` (`Responsable_idResponsable`),
  KEY `fk_Personnel_Parametre1_idx` (`Parametre_idParametre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `responsable`
--

DROP TABLE IF EXISTS `responsable`;
CREATE TABLE IF NOT EXISTS `responsable` (
  `idResponsable` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idResponsable`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_name` varchar(30) NOT NULL,
  `ID_user` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(30) NOT NULL,
  `role_user` varchar(20) NOT NULL,
  PRIMARY KEY (`ID_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `engins_personnel`
--
ALTER TABLE `engins_personnel`
  ADD CONSTRAINT `fk_Engins_has_Personnel_Engins1` FOREIGN KEY (`Engins_idEngins`) REFERENCES `engins` (`idEngins`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Engins_has_Personnel_Personnel1` FOREIGN KEY (`Personnel_idPersonnel`) REFERENCES `personnel` (`idPersonnel`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `intervention`
--
ALTER TABLE `intervention`
  ADD CONSTRAINT `fk_Intervention_Geographique1` FOREIGN KEY (`Geographique_idGeographique`) REFERENCES `geographique` (`idGeographique`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `intervention_engins`
--
ALTER TABLE `intervention_engins`
  ADD CONSTRAINT `fk_Intervention_has_Engins_Engins1` FOREIGN KEY (`Engins_idEngins`) REFERENCES `engins` (`idEngins`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Intervention_has_Engins_Intervention` FOREIGN KEY (`Intervention_idIntervention`) REFERENCES `intervention` (`idIntervention`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `personnel`
--
ALTER TABLE `personnel`
  ADD CONSTRAINT `fk_Personnel_Parametre1` FOREIGN KEY (`Parametre_idParametre`) REFERENCES `parametre` (`idParametre`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Personnel_Responsable1` FOREIGN KEY (`Responsable_idResponsable`) REFERENCES `responsable` (`idResponsable`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
