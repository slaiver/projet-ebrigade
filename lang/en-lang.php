<?php
 	 
 	 //---------------------------------------------------------
 	 // index.php
 	 //---------------------------------------------------------
 	 
 	  define('TXT_ACCUEIL_INDEX', 'Welcome to Application eBrigade : ');
 	  define('TXT_Code', 'Code');
	  define('TXT_Prenom', 'FirstName');
	  define('TXT_Nom', 'LastName');
	  define('TXT_Civilite', 'Civility');
	  define('TXT_Grade', 'Grade');
	  define('TXT_Profession', 'Profession');
	  define('TXT_Statut', 'Status');
	  define('TXT_Naissance', 'Birthday');
	  define('TXT_Email', 'Mail');
	  define('TXT_Telephone', 'Phone');
	  define('TXT_Ajoute_utilisateur', 'Add User');
	  define('TXT_Affiche_utilisateur', 'Show Users');
	  define('TXT_Param_langue', 'Language Setting');
	  define('TXT_Ajoute_intervention', 'Add Intervention');
	  define('TXT_Affiche_intervention', 'Show Interventions');
	  define('TXT_Utilisateurs', 'Users');
	  define('TXT_Accueil', 'Home');
	  define('TXT_Interventions', 'Interventions');
	  define('TXT_Password', 'Password');
	  define('TXT_Password_Confirmation', 'Password Confirmation');
	  define('TXT_Editer_utilisateur', 'Update User');
	  define('TXT_Ajouter', 'Add');
	  define('TXT_Modifier', 'Update');
  	 
  	 //---------------------------------------------------------
  	 // page1.php
  	 //---------------------------------------------------------
  	 
  	 define('TXT_ACCUEIL_PAGE1', 'Welcome on PHP Débutants first page !');
  	 define('TXT_CONSEIL_PAGE2', 'Install Linux !');
 	 
  	 //---------------------------------------------------------
  	 // Fin
  	 //---------------------------------------------------------
  	 
 	 ?>