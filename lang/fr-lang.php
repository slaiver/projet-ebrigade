 	  <?php
  	 
  	 //---------------------------------------------------------
  	 // index.php
  	 //---------------------------------------------------------
  	 
  	 define('TXT_ACCUEIL_INDEX', 'Bienvenue sur Application eBrigade : ');
	 define('TXT_Code', 'Code');
	 define('TXT_Prenom', 'Prénom');
	 define('TXT_Nom', 'Nom');
	  define('TXT_Civilite', 'Civilité');
	  define('TXT_Grade', 'Grade');
	  define('TXT_Profession', 'Profession');
	  define('TXT_Statut', 'Statut');
	  define('TXT_Naissance', 'Naissance');
	  define('TXT_Email', 'Email');
	  define('TXT_Telephone', 'Telephone');
	  define('TXT_Ajoute_utilisateur', 'Ajouter Utilisateur');
	  define('TXT_Affiche_utilisateur', 'Afficher Utilisateurs');
	  define('TXT_Param_langue', 'Paramètre de Langue');
	  define('TXT_Ajoute_intervention', 'Ajouter Intervention');
	  define('TXT_Affiche_intervention', 'Afficher Interventions');
	  define('TXT_Utilisateurs', 'Utilisateurs');
	  define('TXT_Accueil', 'Accueil');
	  define('TXT_Interventions', 'Interventions');
	  define('TXT_Password', 'Mot de Passe');
	  define('TXT_Password_Confirmation', 'Confirmation');
	  define('TXT_Editer_utilisateur', 'Modifier Utilisateur');
	  define('TXT_Ajouter', 'Ajouter');
	  define('TXT_Modifier', 'Modifier');
	  
  	 
  	 //---------------------------------------------------------
  	 // page1.php
  	 //---------------------------------------------------------
  	 
  	 define('TXT_ACCUEIL_PAGE1', 'Bienvenue sur la page 1 du site PHP Débutant !');
  	 define('TXT_CONSEIL_PAGE2', 'Installez Linux !');
  	 
  	 //---------------------------------------------------------
  	 // Fin
  	 //---------------------------------------------------------
  	 
 	 ?>