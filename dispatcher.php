<?php

class Dispatcher
{

    private $request;

    public function dispatch()
    {
        $this->request = new Request();
        Router::parse($this->request->url, $this->request);
        //echo 'action est '.$this->request->action;
        //echo 'contr est '.$this->request->controller;
        $controller = $this->loadController();
        if(empty($this->request->action) )
           $this->request->action= "index";

        //echo $this->request->action;
        call_user_func_array([$controller, $this->request->action], $this->request->params);
    }

    public function loadController()
    {
        $name = $this->request->controller . "Controller";
        $file = ROOT . 'Controllers/' . $name . '.php';
        require($file);
        $controller = new $name();
        return $controller;
    }

}
?>