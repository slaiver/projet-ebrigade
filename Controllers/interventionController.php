<?php
class interventionController extends Controller
{
    function indexInterv()
    {
        require(ROOT . 'Models/intervention.php');

        $Intervention = new Intervention();
        $d['interventions'] = $Intervention->showAllInterv();
        $this->set($d);
        $this->render("indexInterv");
    }

    function createInterv()
    {
        if (isset($_POST["NomIntervention"]))
        {
            require(ROOT . 'Models/intervention.php');

            $Intervention = new Intervention();
            if ($Intervention->create($_POST["NomIntervention"], $_POST["Statut"], $_POST["DateD"], $_POST["HeureD"], $_POST["DateF"],$_POST["HeureF"]))
            {
                header("Location: " . WEBROOT . "intervention/indexInterv");
            }
        }

        $this->render("createInterv");
    }

    function edit($Id)
    {
        require(ROOT . 'Models/intervention.php');
        $Intervention = new Intervention();

        $d["intervention"] = $Intervention->showInterv($Id);
        //echo $d;
        if (isset($_POST["NomIntervention"]))
        {
            if ($Intervention->edit($Id,$_POST["NomIntervention"], $_POST["Statut"], $_POST["DateD"], $_POST["HeureD"], $_POST["DateF"],$_POST["HeureF"]))
            {
                header("Location: " . WEBROOT . "intervention/indexInterv");
            }
        }
        $this->set($d);
        $this->render("edit");
    }

    function delete($Id)
    {
        require(ROOT . 'Models/intervention.php');
        $Intervention = new Intervention();
        if ($Intervention->delete($Id))
        {
            
            header("Location: " . WEBROOT . "intervention/indexInterv");
        }
    }

}
?>