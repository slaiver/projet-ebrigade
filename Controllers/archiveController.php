<?php
class archiveController extends Controller
{
    function listIntervention()
    {
        $_SESSION['lang']='en';
       require(ROOT . 'Models/intervention.php');

        $Intervention = new Intervention();
        $d['interventions'] = $Intervention->showAllInterv();
        $this->set($d);
        $this->render("listIntervention");
    }

    function viewIntervention($Id)
    {
        $_SESSION['lang']='fr';
        require(ROOT . 'Models/intervention.php');
        $Intervention = new Intervention();

        $d["intervention"] = $Intervention->showInterv($Id);
        $this->set($d);
        $this->render("viewIntervention");
    }

    

    function filtrePar($element)
    {
        require(ROOT . 'Models/ArchiveFiltre.php');
        $archive = new ArchiveFiltre();
        switch ($element) {
            case 'date': {
                    $name = "Date_Heure_Debut";
                    $column = "Date et Heure de déclenchement";
                    break;
                }
            case 'nIntervention': {
                    $name = "Numero_Intervention";
                    $column = "Numero d'intervention";
                    break;
                }

            case 'motif': {
                    $name = "Type_interv";
                    $column = "Motif de l'intervention";
                    break;
                }
            case 'adresse': {
                    $name = "Adresse";
                    $column = "Adresse de l'intervention";
                    break;
                }
            case 'vehicule': {
                    $name = "Nom_Engin";
                    $column = "Nom de la voiture";
                    break;
                }
        }

        $d["element"] = $element;
        $d["name"] = $name;
        $d["column"] = $column;

        if ($element == 'vehicule')
            $d["interventions"] = $archive->showfiltrevoiture($name);
        else
            $d["interventions"] = $archive->showfiltree($name);

        $this->set($d);
        $this->render("listInterventionFiltre");
    }
}
