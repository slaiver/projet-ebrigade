<?php

class loginmodel
{
   private $username;
   private $password;
   private $cnx;   //database object 

   function __construct($username,$password)
   {
     //set data
       $this->setData($username,$password);
     //connect DB
       $this->connetToDb();  
   }
   private function setData($username,$password)
   {
      $this->username=$username;
      $this->password=$password;
   }
   private function connetToDb()
   {
    $this->cnx = new PDO('mysql:host=localhost;dbname=dbebrigade','root','');
    
   }
   public function getData()
   {      
           $data=[$this->username,md5($this->password)];
           $req = $this->cnx->prepare("SELECT * FROM users WHERE username=? AND password=?");
           $req->execute($data);
           return  $req->rowCount(); 
   }  
   function close()
   {
       $this->cnx->close();
   }
}
?>