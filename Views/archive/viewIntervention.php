
<main role="main" class="container">
    <div class="starter-template">
      <h1>Affichage d'une intervention</h1>
    </div>

    <br>
  <br/>
  <div class="row">
    <h3>
    <?php if (isset($intervention["Numero_Intervention"])) echo $intervention["Numero_Intervention"];?>
    </h3>
  </div><br>

  <div class="row">
    <label class="col-md-4 control-label">Statut :</label>
    <div class="col-md-8">
    <?php if (isset($intervention["Statut"])) echo $intervention["Statut"];?>
    </div>
  </div>
  <div class="row">
    <label class="col-md-4 control-label">Date de début :</label>
    <div class="col-md-8">
    <?php if (isset($intervention["DateD"])) echo $intervention["DateD"];?>
    </div>
  </div>
  <div class="row">
    <label class="col-md-4 control-label">Heure de début :</label>
    <div class="col-md-8">
    <?php if (isset($intervention["HeureD"])) echo $intervention["HeureD"];?>
    </div>
  </div>
  <div class="row">
    <label class="col-md-4 control-label">Date de fin :</label>
    <div class="col-md-8">
    <?php if (isset($intervention["DateF"])) echo $intervention["DateF"];?>
    </div>
  </div>
  <div class="row">
    <label class="col-md-4 control-label">Heure de fin :</label>
    <div class="col-md-8">
    <?php if (isset($intervention["HeureF"])) echo $intervention["HeureF"];?>
    </div>
  </div>



</main><!-- /.container -->