<h1>Archive des interventions</h1><br>


<div classe="float-right">
    <label for="filtre-select">Filtré par : </label>&nbsp;&nbsp;&nbsp;
    
    <select onchange="javascript:location.href = this.value;">
        <option value="">-- Choisir une option --</option>
        <option value="/AppMvc/archive/filtrePar/date">Date</option>
        <option value="/AppMvc/archive/filtrePar/motif">Motif</option>
        <option value="/AppMvc/archive/filtrePar/vehicule">Véhicule</option>
        <option value="/AppMvc/archive/filtrePar/Numéro d’intervention">Numéro d’intervention</option>
        <option value="/AppMvc/archive/filtrePar/adresse">Adresse</option>
        <option value="/AppMvc/archive/filtrePar/redacteur">Rédacteur</option>
    </select>

</div><br>




<div class="row col-md-12 centered">
    <table class="table table-striped custab">
        <thead>

            <tr>
                <th>Nom d'intervention</th>
                <th>Statut</th>
                <th>Date de début</th>
                <th>Heure de début</th>
                <th>Date de fin</th>
                <th>Heure de fin</th>

            </tr>
        </thead>


        <?php
        $var = '';
        foreach ($interventions as $intervention) {
            $txt = '';

            echo "<tr onclick=\"window.location='/AppMvc/archive/viewIntervention/" . $intervention["idIntervention"] . "';\">";

            //echo "<td>" . $intervention['IdInterv'] . "</td>";
            $txt .= "<td>" . $intervention['Numero_Intervention'] . "</td>";
            $txt .= "<td>" . $intervention['Statut'] . "</td>";
            $txt .= "<td>" . $intervention['Adresse'] . "</td>";
            $txt .= "<td>" . $intervention['Date_Heure_Debut'] . "</td>";
            $txt .= "<td>" . $intervention['Date_Heure_Fin'] . "</td>";
            $txt .= "<td>" . $intervention['Opm'] . "</td>";
            $txt .= "</tr>";
            echo $txt;
            $var .= "<tr>" . $txt;
        }
        ?>
    </table>
</div>

<form action="/AppMvc/export/listIntervention" method="POST">

    <input type="hidden" name="var" value="<?php echo $var; ?>">
    <input type="submit" value="entrer">

</form>