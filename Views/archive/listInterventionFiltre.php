<h1>Archive des interventions</h1><br>


<div classe="float-right">
    <label for="filtre-select">Filtré par : </label>&nbsp;&nbsp;&nbsp;
    
    <select onchange="javascript:location.href = this.value;" >
        <option value="">-- Choisir une option --</option>
        <option <?php if($element=='date') echo "selected";?> value="/AppMvc/archive/filtrePar/date">Date</option>
        <option <?php if($element=='motif') echo "selected";?> value="/AppMvc/archive/filtrePar/motif">Motif</option>
        <option <?php if($element=='vehicule') echo "selected";?> value="/AppMvc/archive/filtrePar/vehicule">Véhicule</option>
        <option <?php if($element=='nIntervention') echo "selected";?> value="/AppMvc/archive/filtrePar/nIntervention">Numéro d’intervention</option>
        <option <?php if($element=='adresse') echo "selected";?> value="/AppMvc/archive/filtrePar/adresse">Adresse</option>
        <option <?php if($element=='redacteur') echo "selected";?> value="/AppMvc/archive/filtrePar/redacteur">Rédacteur</option>
    </select>

</div><br>




<div class="row col-md-12 centered">
    <table class="table table-striped custab">
        <thead>

            <tr>
                <th><?php if(isset($column)) echo $column; ?></th>
                

            </tr>
        </thead>


        <?php
        $var = '';
        
       foreach ($interventions as $intervention) {
            $txt = '';

            echo "<tr onclick=\"window.location='/AppMvc/archive/viewIntervention/" . $intervention["idIntervention"] . "';\">";

            //echo "<td>" . $intervention['IdInterv'] . "</td>";
            $txt .= "<td>" . $intervention[$name] . "</td></tr>";
            echo $txt;
            $var .= "<tr>" . $txt;
        }
        ?>
    </table>
</div>

<form action="/AppMvc/export/listIntervention" method="POST">

    <input type="hidden" name="var" value="<?php echo $var; ?>">
    <input type="submit" value="entrer">

</form>