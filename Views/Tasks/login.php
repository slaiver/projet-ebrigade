
<html lang="en">
<head>
	<title>Login Page</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="/APPMVC/Views/images/icons/favicon.ico" />
<!--============================================="APPMVC/Views/Layouts/" . $this->layout . '.php'==================================================-->
	<link rel="stylesheet" type="text/css" href="/APPMVC/Views/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/APPMVC/Views/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/APPMVC/Views/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/APPMVC/Views/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="/APPMVC/Views/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/APPMVC/Views/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/APPMVC/Views/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="/APPMVC/Views/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/APPMVC/Views/css/util.css">
	<link rel="stylesheet" type="text/css" href="/APPMVC/Views/css/main.css">
<!--===============================================================================================-->
</head>
<body >

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form" method='post' action='#'>
					<span class="login100-form-title p-b-34">
						 Page de Connexion
						 <?php if(isset($msg)){ ?><div class="alert alert-danger" role="alert">
						  <?php echo $msg; ?> </div><?php } ?>					                    
					</span>
					

					<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Email">
						<input id="Email" class="input100" type="text" name="Email" placeholder="Entrer votre Email">
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 rs2-wrap-input100 validate-input m-b-20" data-validate="Password">
						<input class="input100" type="Password" name="Password" placeholder="Password">
						<span class="focus-input100"></span>
					</div>
					<div class="container-login100-form-btn">
						<button  type="submit" name="Login" class="login100-form-btn" >
							Connexion
						</button>
					</div>

					<div class="w-full text-center p-t-27 p-b-239">
						<span class="txt1">
							Oublier
						</span>

						<a href="/AppMvc/tasks/motdepass" class="txt2">
							Email / Password?
							<br> <br> <br> 
							Inscription
					
						</a>
					</div>
					
				</form>
				<div class="login100-more" style="background-image: url('/APPMVC/Views/img/pompier2.jpg');"></div>
			</div>
		</div>
		<div >

					
	</div>
	
	


	
<!--===============================================================================================-->
	<script src= "/APPMVC/Views/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src= "/APPMVC/Views/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src= "/APPMVC/Views/vendor/bootstrap/js/popper.js"></script>
	<script src= "/APPMVC/Views/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src= "/APPMVC/Views/vendor/select2/select2.min.js"></script>
	<!-- <script>
		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script> -->
<!--===============================================================================================-->
	<script src="/APPMVC/Views/vendor/daterangepicker/moment.min.js"></script>
	<script src="/APPMVC/Views/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="/APPMVC/Views/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="/APPMVC/Views/js/main.js"></script>

</body>
</html>
