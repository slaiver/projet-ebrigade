<?php
  	 require(ROOT . 'lang/decide-lang.php');
  ?>
<h1><?php echo TXT_Editer_utilisateur; ?></h1>
<form method='post' action='#' style="width:1000px;border: 2px solid #6495ED;border-radius: 20px;padding:40px;">

    <div class="form-group">
        <label for="title"><?php echo TXT_Code; ?></label>
        <input type="text" class="form-control" id="P_CODE" placeholder="<?php echo TXT_Code; ?>..." name="P_CODE" value ="<?php if (isset($task["P_CODE"])) echo $task["P_CODE"];?>">
    </div>

    <div class="form-group">
        <label for="title"><?php echo TXT_Nom; ?></label>
        <input type="text" class="form-control" id="P_NOM" placeholder="<?php echo TXT_Nom; ?>..." name="P_NOM" value ="<?php if (isset($task["P_NOM"])) echo $task["P_NOM"];?>">
    </div>

    <div class="form-group">
        <label for="description"><?php echo TXT_Prenom; ?></label>
        <input type="text" class="form-control" id="P_PRENOM" placeholder="<?php echo TXT_Prenom; ?>..." name="P_PRENOM" value ="<?php if (isset($task["P_PRENOM"])) echo $task["P_PRENOM"];?>">
    </div>

    <div class="form-group">
        <label for="description"><?php echo TXT_Email; ?></label>
        <input type="text" class="form-control" id="P_EMAIL" placeholder="<?php echo TXT_Email; ?>..." name="P_EMAIL" value ="<?php if (isset($task["P_EMAIL"])) echo $task["P_EMAIL"];?>">
    </div>

    <div class="form-group">
        <label for="description"><?php echo TXT_Civilite; ?></label>
        <input type="text" class="form-control" id="P_CIVILITE" placeholder="<?php echo TXT_Civilite; ?>..." name="P_CIVILITE" value ="<?php if (isset($task["P_CIVILITE"])) echo $task["P_CIVILITE"];?>">
    </div>

    <div class="form-group">
        <label for="title"><?php echo TXT_Grade; ?></label>
        <input type="text" class="form-control" id="P_GRADE" placeholder="<?php echo TXT_Grade; ?>..." name="P_GRADE" value ="<?php if (isset($task["P_GRADE"])) echo $task["P_GRADE"];?>">
    </div>

    <div class="form-group">
        <label for="title"><?php echo TXT_Profession; ?></label>
        <input type="text" class="form-control" id="P_PROFESSION" placeholder="<?php echo TXT_Profession; ?>..." name="P_PROFESSION" value ="<?php if (isset($task["P_PROFESSION"])) echo $task["P_PROFESSION"];?>">
    </div>

    <div class="form-group">
        <label for="title"><?php echo TXT_Statut; ?></label>
        <input type="text" class="form-control" id="P_STATUT" placeholder="<?php echo TXT_Statut; ?>..." name="P_STATUT" value ="<?php if (isset($task["P_STATUT"])) echo $task["P_STATUT"];?>">
    </div>

    <div class="form-group">
        <label for="title"><?php echo TXT_Naissance; ?></label>
        <input type="date" class="form-control" id="P_BIRTHDATE" placeholder="<?php echo TXT_Naissance; ?>..." name="P_BIRTHDATE" value ="<?php if (isset($task["P_BIRTHDATE"])) echo $task["P_BIRTHDATE"];?>">
    </div>

    <div class="form-group">
        <label for="title"><?php echo TXT_Telephone; ?></label>
        <input type="text" class="form-control" id="P_PHONE" placeholder="<?php echo TXT_Telephone; ?>..." name="P_PHONE" value ="<?php if (isset($task["P_PHONE"])) echo $task["P_PHONE"];?>">
    </div>
    <button type="submit" class="btn btn-primary" style="width:100px;margin-left:400px;"><?php echo TXT_Modifier; ?></button>
</form>