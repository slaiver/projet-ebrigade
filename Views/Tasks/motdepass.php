<?php
    session_start(); 
    include("bd/connexionDB.php");
 
    if (isset($_SESSION["id"])){
        header("Location: index.php");
        exit;
    }
 
    if(!empty($_POST)){
        extract($_POST);
        $valid = true;
 
        if (isset($_POST["oublie"])){
            $mail = htmlentities(strtolower(trim($mail))); // On récupère le mail afin d envoyer le mail pour la récupèration du mot de passe 
 
            // Si le mail est vide alors on ne traite pas
            if(empty($mail)){
                $valid = false;
                $er_mail = "Il faut mettre un mail";
            }
 
            if($valid){
                $verification_mail = $DB->query("SELECT P_NOM, P_PRENOM, P_EMAIL, P_PASSWORD_FAILURE 
                    FROM pompier WHERE P_EMAIL = ?",
                    array($mail));
                $verification_mail = $verification_mail->fetch();
 
                if(isset($verification_mail["P_EMAIL"])){
                    if($verification_mail["P_PASSWORD_FAILURE"] == 0){
                        // On génère un mot de passe à l"aide de la fonction RAND de PHP
                        $new_pass = rand();
 
                        // Le mieux serait de générer un nombre aléatoire entre 7 et 10 caractères (Lettres et chiffres)
                        $new_pass_crypt = crypt($new_pass, "$6$rounds=5000$macleapersonnaliseretagardersecret$");
                        // $new_pass_crypt = crypt($new_pass, "VOTRE CLÉ UNIQUE DE CRYPTAGE DU MOT DE PASSE");
 
                        $objet = "Nouveau mot de passe";
                        $to = $verification_mail["P_EMAIL"];
 
                        //===== Création du header du mail.
                        $header = "From: NOM_DE_LA_PERSONNE <no-reply@test.com> \n";
                        $header .= "Reply-To: ".$to."\n";
                        $header .= "MIME-version: 1.0\n";
                        $header .= "Content-type: text/html; charset=utf-8\n";
                        $header .= "Content-Transfer-Encoding: 8bit";
 
                        //===== Contenu de votre message
                        $contenu =  "<html>".
                            "<body>".
                            "<p style="text-align: center; font-size: 18px"><b>Bonjour Mr, Mme".$verification_mail["P_NOM"]."</b>,</p><br/>".
                            "<p style="text-align: justify"><i><b>Nouveau mot de passe : </b></i>".$new_pass."</p><br/>".
                            "</body>".
                            "</html>";
                        //===== Envoi du mail
                        mail($to, $objet, $contenu, $header);
                        $DB->insert("UPDATE pompier SET P_MDP = ?, P_PASSWORD_FAILURE = 1 WHERE P_EMAIL = ?", 
                            array($new_pass_crypt, $verification_mail["P_EMAIL"]));
                    }   
                }       
                header("Location: " . WEBROOT . "tasks/login");
                exit;
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="fr">
<head>
<title>Mot de passe oublié</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="/APPMVC/Views/images/icons/favicon.ico" />
<!--============================================="APPMVC/Views/Layouts/" . $this->layout . ".php"==================================================-->
	<link rel="stylesheet" type="text/css" href="/APPMVC/Views/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/APPMVC/Views/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/APPMVC/Views/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/APPMVC/Views/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="/APPMVC/Views/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/APPMVC/Views/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/APPMVC/Views/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="/APPMVC/Views/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/APPMVC/Views/css/util.css">
	<link rel="stylesheet" type="text/css" href="/APPMVC/Views/css/main.css">
<!--===============================================================================================-->
</head>
        
   
    <body>
<div class="limiter">
		<div class="container-login100">
			<div >
				<form class="login900-form validate-form" method="post" action="#" style="width:500px;border: 2px solid green;border-radius: 50px;">
					<span class="login100-form-title p-b-34">
                    <div>Mot de passe oublié</div>
                    </span>
                     <?php if(isset($er_mail)){ 
                    ?>
                    <div>
                    <?= $er_mail ?>
                    </div>
                    <?php 
                     } ?>
                    <div class="wrap-input100 rs1-wrap-input250 validate-input m-b-20" data-validate="Email">
                    <input id="Email" type="email" class="input100" type="text" name="mail"value="<?php if(isset($mail)){ echo $mail; }?>" required placeholder="Entrer votre Email" >
                    <span class="focus-input100"></span>
                    </div>
                    <button class="wrap-input100 rs1-wrap-input250 validate-input m-b-20" type="submit" name="oublie">Envoyer</button>

                    </form>
</div>
</div>
</div>
	
<!--===============================================================================================-->
<script src= "/APPMVC/Views/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src= "/APPMVC/Views/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src= "/APPMVC/Views/vendor/bootstrap/js/popper.js"></script>
	<script src= "/APPMVC/Views/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src= "/APPMVC/Views/vendor/select2/select2.min.js"></script>
	<!-- <script>
		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $("#dropDownSelect1")
		});
	</script> -->
<!--===============================================================================================-->
	<script src="/APPMVC/Views/vendor/daterangepicker/moment.min.js"></script>
	<script src="/APPMVC/Views/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="/APPMVC/Views/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="/APPMVC/Views/js/main.js"></script>
    </body>
</html>



           