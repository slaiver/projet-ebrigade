
<?php
 include('header.php');
 include('navbar.php');
?>
<main role="main" class="container">

    <div class="starter-template">

        <?php
        
        include('topnav.php');
        echo $content_for_layout; 
        
      
        ?>

    </div>

</main>
<?php
  include('scripts.php');
  include('footer.php');
  
?>