  <!-- Bootstrap core JavaScript-->
  <script src="/APPMVC/Views/vendor/jquery/jquery.min.js"></script>
  <script src="/APPMVC/Views/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="/APPMVC/Views/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="/APPMVC/Views/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="/APPMVC/Views/vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="/APPMVC/Views/js/demo/chart-area-demo.js"></script>
  <script src="/APPMVC/Views/js/demo/chart-pie-demo.js"></script>
