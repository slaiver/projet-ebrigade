<h1>Editer une intervention</h1>
<form method='post' action='#'>
    <div class="form-group">
        <label for="title">Nom Intervention</label>
        <input type="text" class="form-control" id="NomIntervention" placeholder="Entrer le Nom" name="NomIntervention"
         value ="<?php if (isset($intervention["NomIntervention"])) echo $intervention["NomIntervention"];?>">
    </div>

    <div class="form-group">
        <label for="description">Statut</label>
        <input type="text" class="form-control" id="Statut" placeholder="Entrer le Statut" name="Statut"
        value ="<?php if (isset($intervention["Statut"])) echo $intervention["Statut"];?>">

    </div>

    <div class="form-group">
        <label for="description">Date de début</label>
        <input type="text" class="form-control" id="DateD" placeholder="Entrer la date de début" name="DateD"
        value ="<?php if (isset($intervention["DateD"])) echo $intervention["DateD"];?>">
    </div>

    <div class="form-group">
        <label for="description">Heure de début</label>
        <input type="text" class="form-control" id="HeureD" placeholder="Entrer l'Heure de début'" name="HeureD"
        value ="<?php if (isset($intervention["HeureD"])) echo $intervention["HeureD"];?>">
    </div>

    <div class="form-group">
        <label for="description">Date de fin</label>
        <input type="text" class="form-control" id="DateF" placeholder="Entrer la Date de fin" name="DateF"
        value ="<?php if (isset($intervention["DateF"])) echo $intervention["DateF"];?>">
    </div>

    <div class="form-group">
        <label for="description">Heure de fin</label>
        <input type="text" class="form-control" id="HeureF" placeholder="Entrer l'Heure de fin" name="HeureF"
        value ="<?php if (isset($intervention["HeureF"])) echo $intervention["HeureF"];?>">
    </div>
    
    <button type="submit" class="btn btn-primary">Valider</button>
</form>