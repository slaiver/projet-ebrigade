<h1>Gestion des interventions</h1>
<br>
<a href="/AppMvc/intervention/createInterv/" class="btn btn-primary btn-xs pull-right">
        <b>+</b>Ajouter une nouvelle intervention</a>
        <br><br><br>
<div class="row col-md-12 centered">
    <table class="table table-striped custab">
        <thead>
        
        
        <tr>
            <th>Nom d'intervention</th>
            <th>Statut</th>
            <th>Date de début</th>
            <th>Heure de début</th>
            <th>Date de fin</th>
            <th>Heure de fin</th>
            <th class="text-center">Action</th>
        </tr>
        </thead>
        <?php
    

        foreach ($interventions as $intervention)
        {
            echo '<tr>';
            //echo "<td>" . $intervention['IdInterv'] . "</td>";
            echo "<td>" . $intervention['NomIntervention'] . "</td>";
            echo "<td>" . $intervention['Statut'] . "</td>";
            echo "<td>" . $intervention['DateD'] . "</td>";
            echo "<td>" . $intervention['HeureD'] . "</td>";
            echo "<td>" . $intervention['DateF'] . "</td>";
            echo "<td>" . $intervention['HeureF'] . "</td>";
            echo "<td class='text-center'><a class='btn btn-info btn-xs' href='/AppMvc/intervention/edit/" . $intervention["IdInterv"] . "' >
            <span class='glyphicon glyphicon-edit'></span> Edit</a> <a href='/AppMvc/intervention/delete/" . $intervention["IdInterv"] . 
            "' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove'></span> Del</a></td>";
            echo "</tr>";
        }
        ?>
    </table>
</div>