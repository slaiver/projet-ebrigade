<h1>Créer une intervention</h1>
<form method='post' action='#'>
    <div class="form-group">
        <label for="title">Nom Intervention</label>
        <input type="text" class="form-control" id="NomIntervention" placeholder="Entrer le Nom" name="NomIntervention">
    </div>

    <div class="form-group">
        <label for="description">Statut</label>
        <input type="text" class="form-control" id="Statut" placeholder="Entrer le Statut" name="Statut">
    </div>

    <div class="form-group">
        <label for="description">Date de début</label>
        <input type="text" class="form-control" id="DateD" placeholder="Entrer la Date" name="DateD">
    </div>

    <div class="form-group">
        <label for="description">Heure de début</label>
        <input type="text" class="form-control" id="HeureD" placeholder="Entrer l'Heure" name="HeureD">
    </div>

    <div class="form-group">
        <label for="description">Date de fin</label>
        <input type="text" class="form-control" id="DateF" placeholder="Entrer la Date de fin" name="DateF">
    </div>

    <div class="form-group">
        <label for="description">Heure de fin</label>
        <input type="text" class="form-control" id="HeureF" placeholder="Entrer l'Heure de fin" name="HeureF">
    </div>
    
    <button type="submit" class="btn btn-primary">Submit</button>
</form>