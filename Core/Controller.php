<?php
    class Controller
    {
        
        var $vars = [];
        var $layout = "default";

        function set($d)
        {
            $this->vars = array_merge($this->vars, $d);
            //print_r ($this->vars);
        }

        function render($filename)
        {
            include "../Webroot/config.php";
            
            //echo "   ".$filename;
            extract($this->vars);
            ob_start();
            //echo "Views/" . ucfirst(str_replace('Controller', '', get_class($this))) . '/' . $filename . '.php';
            if($filename=='login' || $filename=='motdepass')
            {
                require(ROOT . "Views/" . ucfirst(str_replace('Controller', '', get_class($this))) . '/' . $filename . '.php');
            }
            else
            {   
                require(ROOT . "Views/" . ucfirst(str_replace('Controller', '', get_class($this))) . '/' . $filename . '.php');         
                $content_for_layout = ob_get_clean();

                if ($this->layout == false)
                {
                    $content_for_layout;
                    
                }
                else
                {
                    require(ROOT . "Views/Layouts/" . $this->layout . '.php');
                    
                }
              }
        }
        function renderParam($filename,$d)
        { 
            $this->vars = array_merge($this->vars, $d);

            extract($this->vars);
            ob_start();
            require(ROOT . "Views/" . ucfirst(str_replace('Controller', '', get_class($this))) . '/' . $filename . '.php');
            $content_for_layout = ob_get_clean();

            if ($this->layout == false)
            {
                $content_for_layout;
            }
            else
            {
                require(ROOT . "Views/Layouts/" . $this->layout . '.php');
            }
        }
        private function secure_input($data)
        {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }

        protected function secure_form($form)
        {
            foreach ($form as $key => $value)
            {
                $form[$key] = $this->secure_input($value);
            }
        }

    }
?>